advi (2.0.0-4) unstable; urgency=medium

  * Use FORCE_SOURCE_DATE for dvips reproducibility

 -- Stéphane Glondu <glondu@debian.org>  Mon, 26 Aug 2024 02:54:13 +0200

advi (2.0.0-3) unstable; urgency=medium

  * Use SOURCE_DATE_EPOCH to set the date in manual

 -- Stéphane Glondu <glondu@debian.org>  Sun, 25 Aug 2024 06:37:01 +0200

advi (2.0.0-2) unstable; urgency=medium

  * Use SOURCE_DATE_EPOCH for reproducibility

 -- Stéphane Glondu <glondu@debian.org>  Sat, 24 Aug 2024 06:51:17 +0200

advi (2.0.0-1) unstable; urgency=medium

  * New upstream release
  * Fix compilation with OCaml 5.2.0
  * Bump Standards-Version to 4.7.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 10 Aug 2024 05:18:27 +0200

advi (1.10.2-10) unstable; urgency=medium

  * Depend on ocaml instead of transitional ocaml-nox

 -- Stéphane Glondu <glondu@debian.org>  Tue, 12 Sep 2023 07:51:10 +0200

advi (1.10.2-9) unstable; urgency=medium

  * Add libgraphics-ocaml-dev to Build-Depends, fix its detection by
    configure script

 -- Stéphane Glondu <glondu@debian.org>  Thu, 15 Oct 2020 11:15:03 +0200

advi (1.10.2-8) unstable; urgency=medium

  * Fix compilation with camlimages 5.0.3

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Aug 2020 10:40:44 +0200

advi (1.10.2-7) unstable; urgency=medium

  * Add empty override_dh_dwz to prevent FTBFS (Closes: #968225)
  * Bump debhelper compat level to 13
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 11 Aug 2020 09:49:06 +0200

advi (1.10.2-6) unstable; urgency=medium

  * Add build-dependency texlive-extra-utils (closes: #951922)
  * Build-depend on debhelper-compat, drop file debian/compat
  * Debhelper compatibility level 12
  * Rename debian/TODO.Debian to debian/TODO
  * Standards-Version 4.5.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Sun, 23 Feb 2020 22:03:49 +0100

advi (1.10.2-5) unstable; urgency=medium

  * Fix compilation with OCaml 4.08.0
  * Remove Samuel from Uploaders

 -- Stéphane Glondu <glondu@debian.org>  Thu, 05 Sep 2019 13:36:53 +0200

advi (1.10.2-4) unstable; urgency=medium

  * Update Vcs-* to salsa
  * Standards-Version 4.3.0
    - d/copyright: https in format header
  * patch examples-scripts: fix spelling error in description
  * Debhelper compatibility level 11
    - d/rules: override dh_autoreconf
  * Remove trailing whitespace in this file.
  * d/copyright: change pointer to complete license from LGPL to LGPL-2.1

 -- Ralf Treinen <treinen@debian.org>  Fri, 25 Jan 2019 08:09:34 +0100

advi (1.10.2-3) unstable; urgency=low

  [Rémi Vanicat]
  * removing myself from uploaders

  [Ralf Treinen]
  * patch safe: make Safer mode the default (closes: #583994)
  * advi-examples: drop spurious Suggests tk4.8 et mpg321
  * Standards-Version 3.9.8 (no change)
  * advi-examples: drop Conflicts: advi (<< 1.6.0-4) since that version is
    more than 10 years old.
  * advi: drop "Replaces: activedvi,mldvi" since these are previous names
    of this package last used in 2002, resp. 2001.
  * advi: don't create empty directory usr/share/lintian/overrides/
  * d/control: fix Vcs-* fields, use secure URI
  * patch drop-build-date: make build reproducible (closes: #834137)
  * debhelper compat version 9
  * fix a spelling error in debian/README.

 -- Ralf Treinen <treinen@debian.org>  Mon, 23 Jan 2017 21:23:56 +0100

advi (1.10.2-2) unstable; urgency=low

  [ Ralf Treinen ]
  * Fix mode of /usr/share/doc/advi-examples/examples/basics/alltt.sty
  * debian/rules: use $(OCAML_HAVE_OCAMLOPT) to distinguish native
    from non-native architectures.
  * Fix epoch in build-dependency on libcamlimages-ocaml-dev (thanks to
    Jakub Wilk).
  * Standards-version 3.9.4
    - migrate debian/policy to machine-readable format 1.0
  * fix installation of latex and hevea files:
    - debian/advi.dirs: create directories for hevea and latex stuff
    - debian/advi.install: install hevea and tex files in the right directories
    - debian/rules: remove top-level /advi file created by a buggy
      installation script (closes: #726418)
  * Drop obsolete lintian override

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

 -- Ralf Treinen <treinen@debian.org>  Wed, 16 Oct 2013 22:25:29 +0200

advi (1.10.2-1) unstable; urgency=low

  * New upstream release.
  * patch really-clean: remove some files leftover by upstream clean.
  * debian/rules: fix name of target override_dh_auto_test

 -- Ralf Treinen <treinen@debian.org>  Fri, 21 Oct 2011 16:43:53 +0200

advi (1.10.0-2) unstable; urgency=low

  * debian/control and debian/jpfonts.conf: replaced ttf-kochi- fonts
    (closes: #644772). Thanks to Hideki Yamane for the patch!
  * Add a README to debian/examples/japanese that explains what
    additional packages are needed.

 -- Ralf Treinen <treinen@debian.org>  Thu, 13 Oct 2011 15:32:27 +0200

advi (1.10.0-1) unstable; urgency=low

  * New upstream version. This version does no longer use -custom for
    bytecode compilation (closes:  #517546).
    - drop patch linker_as_needed since now integrated by upstream
    - drop patch camlimages-4.0.1 since now integrated by upstream
  * We don't need to override dh_strip anymore since the -custom option
    is no longer used.
  * $(MAKE) doc still doesn't work:
    - patch dont-make-doc: do not attempt to invoke make on doc/
    - install manpage through debian/advi.manpages
    - install user documentation through debian/advi.install

 -- Ralf Treinen <treinen@debian.org>  Tue, 02 Aug 2011 19:11:34 +0200

advi (1.9.0-5) unstable; urgency=low

  [ Stéphane Glondu ]
  * Adapt to camlimages 4.0.1
  * Remove Stefano from Uploaders

  [ Ralf Treinen ]
  * debian/rules: configure with --sysconfdir=/etc. This fixes a bug
    where advi did not find the system configuration file for japanese
    fonts (closes: 631811).
  * Add to the advi-examples package the simple japanese test file.
    Thanks to Junichi Uekawa for having provided that file.

 -- Ralf Treinen <treinen@debian.org>  Sun, 03 Jul 2011 21:35:23 +0200

advi (1.9.0-4) unstable; urgency=low

  * Change dependency on ghostscript to ghostscript-x (closes: #618270).
  * Remove build-dependency on ocaml-nox since lintian is fixed.
  * Patch linker_as_needed: fix FTBFS with linker flag --as-needed
    (closes:  #607530). Thanks a lot to Stefan Potyra for the patch!
  * Standard-Version 3.9.2 (no change)
  * do not start short description on an article.
  * Patch examples-scripts: call directly perl interpreter in
    examples/prosper/LL/pdfrotate.pl. Add dependency of advi-examples
    on perl.

 -- Ralf Treinen <treinen@debian.org>  Sun, 01 May 2011 14:38:45 +0200

advi (1.9.0-3) unstable; urgency=low

  * Add build-dependency on ocaml-nox to bypass lintian bug #612568
    (wrongly complaining about a missing build-dependency). This should be
    dropped when lintian >= 2.5.0~rc1 hits unstable.
  * Patch examples-makefiles: fix pathnames in examples/basic/Makefile

 -- Ralf Treinen <treinen@debian.org>  Sun, 13 Mar 2011 22:25:59 +0100

advi (1.9.0-2) experimental; urgency=low

  * Removed ghostscript from the dependencies of advi-examples
  * Patch linker_as_needed: enable linking with --as-needed flag. Thanks
    to Stefan Potyra for the  patch! (closes: #607530).
  * Standards-Version 3.9.1 (no change)
  * Versionend dependencies on dh-ocaml and ocaml to have full ocaml
    dependency support.

 -- Ralf Treinen <treinen@debian.org>  Tue, 04 Jan 2011 03:20:35 +0100

advi (1.9.0-1) unstable; urgency=low

  * New upstream version.
  * Compress installed dvi documents, except splash.dvi.
  * Fix dh_strip override: do not strip when non-native architecture.
  * Patches typo-message and manpage: fix some typos.
  * Merged debian/dirs into debian/advi.dirs, removed /usr/share/advi

 -- Ralf Treinen <treinen@debian.org>  Mon, 05 Apr 2010 19:41:22 +0200

advi (1.9.0~rc2-1) experimental; urgency=low

  * New upstream version (release candidat for 1.9.0)
  * Remove patch cache-dir-tilde-expansion, as this is now integrated by
    upstream.
  * Remove patch use-ghostscript as upstream has fixed his configuration
    script. Invoke ./configure with option --with-gs.
  * Removed patch as upstream has updated his manpage, and since zadvi has
    disappeared from he package.
  * Removed zadvi from the package since advi now can directly read
    gzipped dvi files, and removed link for the zadvi manpage. Added
    an entry about this in debian/NEWS.
  * Removed patches use-data-dir, examples-phony-targets, and
    doc-without-htmlc as upstream has fixed his build system.
  * Removed patch japanese-fonts as upstream has fixed this.
  * Install splash dvi douments in /usr/share/doc/advi where advi expects
    them.

 -- Ralf Treinen <treinen@debian.org>  Sun, 04 Apr 2010 20:33:10 +0200

advi (1.9-2) unstable; urgency=low

  * Upload to unstable. Besides, no change w.r.t. 1.9-1.

 -- Ralf Treinen <treinen@debian.org>  Tue, 16 Mar 2010 09:06:47 +0100

advi (1.9-1) experimental; urgency=low

  * New upstream version. This version closes a bug concerning embedded
    postscript images (closes: #452044) and with the scaling of the display
    (closes: #323963).
  * Upstream distributes again the examples/ subtree, install it instead
    of our old copy in debian/examples-source. Remove from
    debian/examples-source the old copies of examples and test/.
  * patch doc-without-htmlc: do not call htmlc for building the doc
  * patch examples-no-check: add phony check target in examples/Makefile
    since this is called recursively on all subdirs from the toplevel
    Makefile.
  * Dependency on ghostscript: >= 7.05
  * Build-dependencies: put back some dependencies since upstream makefile
    does again rebuild the doc: hevea, texlive-latex-base, texlive-pstricks,
    texlive-latex-recommended, texlive-pictures.
  * patch use-ghostscript: make configure believe that ghostscript is installed
    at compilation time.
  * debian/rules: also make the splash.dvi documents in doc/
  * Install doc/{splash,scratch_draw_splash,scratch_write_splash}.dvi files
    into /usr/share/advi
  * debian/control: Updated location of upstream homepage
  * debian/watch: put new domain (advi.inria.fr)

 -- Ralf Treinen <treinen@debian.org>  Mon, 15 Mar 2010 22:03:48 +0100

advi (1.8-2) experimental; urgency=low

  * Bring back changes from 1.6.0-14 and 1.6.0-15 that were accidentially
    ignored when building 1.8-1:
    - dh-ocaml build-dependency >= 0.9
    - uploader Stéphane Glondu

 -- Ralf Treinen <treinen@debian.org>  Tue, 09 Mar 2010 21:27:41 +0100

advi (1.8-1) experimental; urgency=low

  [ Ralf Treinen ]
  * New upstream release (closes: Bug#368830).
  * /etc/advi/jpfonts.conf: use new symlinks for Japanese fonts. Thanks to
    Junichi Uekawa for the patch (closes: Bug#506047).
  * Applied Helge Kreutzmann's patch to the manpage. Thanks, Helge!
    (closes: Bug#286456).
  * Standards-Version 3.8.4 (no change)
  * Rewrote debian/rules from scratch, using dh
  * Build-dependency:
    - remove ocaml-best-compilers: normal ocaml compilers are sufficient
    - remove texlive stuff and hevea: we just use the upstream docs
    - remove ghostscript: not needed
    - add tex-common: needed for installing tex stuff
    - add libfindlib-ocaml: now needed for compilation
    - add dh-ocaml: needed for the ocaml plugin to dh
    - add texlive-binaries: provides kpsexpand
  * Remove debian/post{inst,rm}, use dh_installtex instead
  * Do not try to install the documentation file "Announce"
  * rename debian/docs to debian/advi.docs, links into advi.links
  * debian/copyright: added INRIA as copyright holder
  * Source format 3.0 (quilt)
  * The directories examples/ and test/ are gone from upstream. We put
    these directories from advi-1.6 into debian/examples-source. This
    directory has the relevant debian patches from 1.6.0 applied.
  * New patch:
    - no-local-advirc: do not load .advirc from the working direcory
  * Old patches:
    - 0004-Japanese-fonts: jpfonts.config is no longer in upstream, put it
      into debian/. Patch renamed to japanese-fonts
    - 0005-Cache-dir.patch: renamed to cache-dir-tilde-expansion
    - 0015-Manpage-update.patch: renamed to manpage
    - The following patches are no longe relevant, and have been removed:
        0001-Pull-CVS-HEAD-as-of-20060103 (now integrated in upstream)
        0002-Makefile-.depend-workaround-for-buggy-make-behavior
  	0006-Enable-build-and-install-on-architectures-with-only.patch
        0013-Doc-build.patch (doc recompiles smoothly now)
    - The following patches concern only examples. or test/. They are already
      applied to the copy which now in debian/examples-source. These patches
      are therefore just stored in debian/examples-source/patches:
        0003-Don-t-call-advi-on-example-build-generation
        0007-Fix-examples-includes
        0008-Fix-mon_seminar
        0009-Fix-test-includes.patch
        0010-Neutralize-texinputs.patch
        0011-No-bubble.patch
        0012-Set-test-cache-dir.patch
        0014-xanim-xanimate.patch
        0016-example-makefiles-texinputs.patch
        0017-Don-t-compile-tests-which-fail-with-texlive.patch

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Stephane Glondu ]
  * Remove Julien from Uploaders

 -- Ralf Treinen <treinen@debian.org>  Mon, 08 Mar 2010 20:14:16 +0100

advi (1.6.0-15) unstable; urgency=low

  * Switch to dh-ocaml 0.9
  * debian/control:
    - update my e-mail address, remove DMUA
    - update Standards-Version to 3.8.3 (no changes)
  * Recompile against recent camlimages (Closes: #550440)
    - fixes CVE-2009-2295, CVE-2009-2660, CVE-2009-3296

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Nov 2009 16:47:16 +0100

advi (1.6.0-14) unstable; urgency=low

  [ Ralf Treinen ]
  * Added Homepage field to debian/control file

  [ Sylvain Le Gall ]
  * Update my email adress

  [ Stephane Glondu ]
  * debian/control:
    - clean up Uploaders and add myself there
    - add DM-Upload-Allowed
    - replace obsolete build-dep on x-dev (Closes: #515364)
    - remove dependencies on obsolete packages gs, xbase-clients
    - add build-dependency on dh-ocaml
    - update Standards-Version to 3.8.2
  * debian/rules:
    - rewrite from scratch using dh, bump debhelper compatibility to 7
    - do not compile and install ML examples in advi-examples, as they
      are ABI-dependent and make advi not binNMU-able
    - install examples more consistently
    - install manual in binary package advi
    - use dh_installtex and drop debian/post{inst,rm}; add versioned
      dependency to tex-common
  * Switch packaging to git
  * Switch patches to quilt, add README.source
  * Add a patch to fix build with recent camlimages (Closes: #534020)
  * Fix typo in changelog (s/adress/address/) (thanks Lintian)
  * Switch debian/copyright to new format

 -- Stephane Glondu <steph@glondu.net>  Mon, 13 Jul 2009 23:13:47 +0200

advi (1.6.0-13) unstable; urgency=low

  * Clean enough so that package can be built twice in a row, closes: #424084.
  * Build-depend on texlive instead of tetex, closes: #433584.
  * Added failed-tests.dpatch to remove tests which don't compile anymore with
    texlive.
  * Don't ignore errors on make clean.

 -- Samuel Mimram <smimram@debian.org>  Wed, 18 Jul 2007 15:10:38 +0200

advi (1.6.0-12) unstable; urgency=low

  * Remove inactive people from the Uploaders field.
  * Build with ocamlc instead of ocamlopt on arm to fix build failure.

 -- Julien Cristau <julien.cristau@ens-lyon.org>  Thu, 21 Dec 2006 00:23:24 +0100

advi (1.6.0-11) unstable; urgency=high

  * High-urgency upload to fix FTBFS.

  [ Julien Cristau ]
  * Remove obsolete debian/TODO and debian/doc-base.old.
  * Update watch file to match advi-*.tgz in addition to advi-*.tar.gz.
  * Apply patch by Franz Küster:
    + [patches/doc_build.dpatch] Make the documentation build again (closes:
      #398695)
      - Call imagen explicitly, and use \PassOptionsToPackage instead of
        \def\driver
      - In TeX input files, do not specify the extension (.eps) for included
        images.  This is not needed, and since dvipdfm cannot include eps
        files, it failed.
      - Set TEXINPUTS for dvipdfm, too
    + [patches/build.dpatch,links] Fix configure script to allow for multiple
      TEXMF trees.  TeX input files are installed in TEXMFMAIN, and no link
      will be created (closes: #398922)
    + [rules] Install the manual subdirectory in the advi package, not the
      examples package (closes: #398920)
  * Remove the advi.bin.1.gz symlink, as we don't install advi.bin anymore.

  [ Ralf Treinen ]
  * Weaken dependencies to allow for texlive instead of tetex
    (closes: Bug#388435).

  [ Samuel Mimram ]
  * Update standards version to 3.7.2, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Thu, 23 Nov 2006 21:23:47 +0000

advi (1.6.0-10) unstable; urgency=low

  * Remove work-around against debhelper bug: use -X.aux instead of -X\\.aux,
    etc. Closes: #359302.
  * Corrected typos in TODO.Debian, closes: #286452.

 -- Samuel Mimram <smimram@debian.org>  Tue, 28 Mar 2006 20:54:03 +0000

advi (1.6.0-9) unstable; urgency=low

  * Change the use of conditionals in debian/rules to be compatible with older
    make (Closes: #349776). Thanks, Christian T. Steigies!

 -- Julien Cristau <julien.cristau@ens-lyon.org>  Fri, 27 Jan 2006 11:22:51 +0100

advi (1.6.0-8) unstable; urgency=low

  [ Samuel Mimram ]
  * Adapt the build targets to the new patch.
  * Updated doc_build.dpatch to correctly install the man pages.

  [ Julien Cristau ]
  * Modify debian/rules to try not to build a broken package, and fail if
    there is an error in the build process.

 -- Samuel Mimram <smimram@debian.org>  Tue, 24 Jan 2006 22:26:18 +0000

advi (1.6.0-7) unstable; urgency=low

  * Rebuild for ocaml 3.09.1.
  * Don't build-dep on xlibs-dev, use x-dev, libx11-dev and libxinerama-dev
    instead (Closes: #346608).
  * Add myself and Samuel Mimram to Uploaders.
  * Update to CVS HEAD as of 20060103, and rediff existing patches.
  * Updated standards version to 3.6.2, no changes needed.

 -- Julien Cristau <julien.cristau@ens-lyon.org>  Sat, 21 Jan 2006 15:07:38 +0100

advi (1.6.0-6) unstable; urgency=high

  * Patch examples-makefiles-texinputs: fix TEXINPUTS variable in various
    Makefiles in examples/ such advi styles are found even when the advi
    package is not installed. Fixes FTBFS. (Closes: #307919).
  * Fixed some typos in description, rephrased short and long descriptions a
    bit (closes: #286454).

 -- Ralf Treinen <treinen@debian.org>  Fri,  6 May 2005 20:51:05 +0200

advi (1.6.0-5) unstable; urgency=low

  * Split off examples and manual in their own package (Closes: #233322).
    This includes slight patching for proper include file references;
    also a xanim call is replaced by an animate call.
  * Add README.Debian for the examples package to explain above changes
    and the fact that the video is not included in the example.
  * Mention licences of style files included in the examples-package
  * Don't try to build HTML pages
  * Use ~/.advi as cache for the examples
  * Don't try to build man page, use prebuild one
  * Add support for bz2-files into zadvi (Closes: #286458).
  * Link man page of advi to zadvi and adress this in the man page, this
    makes lintian and linda happy as well (Closes: #286453).
  * Numerous improvements for the man page, adresses #286456. The following
    sub-items are taken care off: a) d) f) g) h) i) j) l) o)

 -- Helge Kreutzmann <kreutzm@itp.uni-hannover.de>  Sat, 29 Jan 2005 17:08:09 +0100

advi (1.6.0-4) unstable; urgency=low

  * Backing up pagemovement patch again, since it broke scaling.
    Reopens #245847.

 -- Sven Luther <luther@debian.org>  Fri, 10 Dec 2004 15:59:21 +0100

advi (1.6.0-3) unstable; urgency=low

  * Added doc/advi.1 manpage installation, thanks to Helge Kreutzmann
    for this. (Closes: #278806)
  * Fixed page movement after scaling, thanks to Yamagata Yoriyuki for
    this. (Closes: #245847)

 -- Sven Luther <luther@debian.org>  Sun, 28 Nov 2004 12:36:45 +0100

advi (1.6.0-2) unstable; urgency=low

  * Forgot to upgrade the camlimages build-dependency, which caused a FTBFS
    because of the Image -> Images module renaming. Fixed now.

 -- Sven Luther <luther@debian.org>  Sat, 23 Oct 2004 21:47:16 +0200

advi (1.6.0-1) unstable; urgency=low

  * New upstream release.
    - Now includes man page. (Closes: #264797, #277778)
    - Exiting scratch mode is possible with q (S mode) or ESC and then qs
      (s mode). (Closes: #274383)
    - Segfault with #257855 provided .dvi file fixed. (Closes: #257855)

 -- Sven Luther <luther@debian.org>  Thu, 21 Oct 2004 10:55:13 +0200

advi (1.5.2+cvs-2004.07.27-5) unstable; urgency=high

  * Now removes .depend in the debian/rules clean target, Allows to build on
    architecture whose buildd need to use sudo instead of fakeroot.

 -- Sven Luther <luther@debian.org>  Mon,  6 Sep 2004 16:37:47 +0200

advi (1.5.2+cvs-2004.07.27-4) unstable; urgency=high

  * Now create dep makefile target in addition to .depend, and explicitly call
    this one, should really (Closes: #265212, #269536).

 -- Sven Luther <luther@debian.org>  Fri,  3 Sep 2004 09:36:15 +0200

advi (1.5.2+cvs-2004.07.27-3) unstable; urgency=high

  * Added explicit make .depend to avoid some strange .depend to be included
    from random places. Thanks to Julien Cristau and Samuel Mimram for
    investigating this. (Closes: #265212, #269536)

 -- Sven Luther <luther@debian.org>  Thu, 12 Aug 2004 12:05:24 +0200

advi (1.5.2+cvs-2004.07.27-2) unstable; urgency=medium

  * Changed build from using .opt compilers if they are available only.
  * Added dependency on ocaml-best-compilers.

 -- Sven Luther <luther@debian.org>  Sun,  8 Aug 2004 16:43:29 +0200

advi (1.5.2+cvs-2004.07.27-1) unstable; urgency=low

  * Used latest CVS version, as 1.4.0 doesn't build with ocaml 3.08. (Closes: #250046)
    - Background enhancement with specification of the geometry and colors.
    - Added an HTML version of the manual with the help of HeVeA and Luc Maranget.
    - Change the background command execution order:
        1) Solid background first,
        2) Then apply the gradient function,
        3) Then draw the image.
      This way you can use an (alpha blended) image on a gradient
    - Long time standing bug of set_title has been fixed: the Active-DVI window
      is now allocated by the Window Manager with the expected correct name.
    - Introducing page timing dump to designated files for synchronisation purposes.
  * Rebuilt for ocaml 3.08.
  * Rebuild against libtiff4. (Closes: #262785)
  * Removed installation of the doc dir.

 -- Sven Luther <luther@debian.org>  Mon,  2 Aug 2004 11:23:46 +0200

advi (1.4.0-7) unstable; urgency=low

  * Correction of the veryclean problem :
     - patch the build before cleaning
     - clean
     - unpatch
    (Really close 225723)
  * Backup a set of files in order to have a proper diff.
  * Move advi.bin to advi and the script shell advi to zadvi
    ( closes: #227267) -- sorry for the mess

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu,  8 Jan 2004 15:03:36 +0100

advi (1.4.0-6) unstable; urgency=low

  * Only copy files that need to be seen in doc ( remove trash from
    latex compilation ) ( closes: #185415 )
  * Register index.html in doc-base ( closes: #185413 )
  * Strip a little the rules ( just basic comment )
  * Correct .gz .bz2 file support ( closes: #210851 ) :
      - Install advi as advi.bin
      - Use a rewriten patch provided by S. Zacchiroli
  * Install test, examples && Makefile.config in /usr/share/doc/advi/examples
    ( closes: #218055 )
  * Applied the patch provided by Stefano for correcting the
    tilde expansion ( cache_dir ) ( closes: #210855 )
  * Move from DH_COMPAT to compat file
  * Remove conffiles since /etc/ files are automatically marked as conffiles
  * Set Maintainer to Debian OCaml Maintainers and uploaders to all of them ;-)

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Wed,  7 Jan 2004 14:15:00 +0100

advi (1.4.0-5) unstable; urgency=low

  * Fixed dpatchification in debian/rules. One patch was affecting the clean
    target, and thus broke the build. (Closes: #225723)

 -- Sven Luther <luther@debian.org>  Thu,  1 Jan 2004 10:39:29 +0100

advi (1.4.0-4) unstable; urgency=low

  * Rebuilt for ocaml 3.07. (Closes: #219545)
  * Dpatchified.

 -- Sven Luther <luther@debian.org>  Mon, 15 Dec 2003 16:31:13 +0100

advi (1.4.0-3) unstable; urgency=low

  * Enabled native built on ia64, as the problems seems to have been worked
    around.

 -- Sven Luther <luther@debian.org>  Sun, 16 Feb 2003 21:50:36 +0100

advi (1.4.0-2) unstable; urgency=low

  * Fixed the documentation fix :(((
  * Added gs dependency and build dependency, needed for showing ps graphics.

 -- Sven Luther <luther@debian.org>  Sat, 15 Feb 2003 21:35:47 +0100

advi (1.4.0-1) unstable; urgency=low

  * New upstream release.
  * Fixed documentation build problem and now also ship .ps and .pdf docs.
    (Closes: #149564)
  * The exam.cls problem does not appear in this version. (Closes: #180087)

 -- Sven Luther <luther@debian.org>  Sat, 15 Feb 2003 13:44:28 +0100

advi (1.2.0-6) unstable; urgency=low

  * Create cache dir in ~/.advi-cache. (Closes: #144464)

 -- Sven Luther <luther@debian.org>  Mon, 23 Dec 2002 19:16:47 +0100

advi (1.2.0-5) unstable; urgency=low

  * Now also ship the Announce file. (Closes: #164299)
  * Now use ttf-kochi-gothic and ttf-kochi-mincho free font packages instead
    of the Microsoft ones provided as upstream default. (Closes: #173027)

 -- Sven Luther <luther@debian.org>  Mon, 16 Dec 2002 23:31:07 +0100

advi (1.2.0-4) unstable; urgency=low

  * Apparently the ia64 fix in 1.2.0-2 did not work, fixed it correctly this
    time, i hope.

 -- Sven Luther <luther@debian.org>  Fri, 08 Nov 2002 22:57:37 +0100

advi (1.2.0-3) unstable; urgency=low

  * Added missing zlib1g-dev Build-dependency. (Closes: #167619)

 -- Sven Luther <luther@debian.org>  Wed, 06 Nov 2002 03:04:34 +0100

advi (1.2.0-2) unstable; urgency=low

  * Disabled the native code built on ia64, since ocamlopt has a problem on
    ia64 with big arrays. (Closes: #164653)
  * Removed again te config.ml target, since it would cause running
    ./configure even during the clean target. (Closes: #164342)

 -- Sven Luther <luther@debian.org>  Mon, 28 Oct 2002 14:36:23 +0100

advi (1.2.0-1) unstable; urgency=low

  * New upstream release.
  * Now don't build depend on libpng2 anymore, since we build depend on
    camlimages which has the libpng dependency. (Closes: #144086)

 -- Sven Luther <luther@debian.org>  Wed, 09 Oct 2002 18:18:43 +0200

advi (1.0.0+cvs-2002.03.26-7) unstable; urgency=high

  * Added tetex-bin dependency (for /usr/bin/kpsewhich).
    (Closes: Bug#145299)

 -- Sven Luther <luther@debian.org>  Thu,  2 May 2002 12:51:11 +0200

advi (1.0.0+cvs-2002.03.26-6) unstable; urgency=high

  * Removed the config.ml target, since it would cause running ./configure
    even during the clean target. We do ./configure anyway at the right time,
    so this was not needed. (Closes: Bug#144236) (trully this time).

 -- Sven Luther <luther@debian.org>  Tue, 30 Apr 2002 12:51:59 +0200

advi (1.0.0+cvs-2002.03.26-5) unstable; urgency=high

  * Removed copying of config.sub/config.guess during the clean target.
   (Closes: Bug#144236)

 -- Sven Luther <luther@debian.org>  Wed, 24 Apr 2002 09:30:03 +0200

advi (1.0.0+cvs-2002.03.26-4) unstable; urgency=high

  * Fixed clear command crash (Closes: Bug#141043).

 -- Sven Luther <luther@debian.org>  Sat, 20 Apr 2002 14:59:28 +0200

advi (1.0.0+cvs-2002.03.26-3) unstable; urgency=high

  * Moved jpfonts.conf to /etc/advi.
  * Fixed the path configuration issue :
    now find jpfonts.conf correctly (Closes: Bug#139578).
    and also the splash.dvi file (Closes: Bug#141043).

 -- Sven Luther <luther@debian.org>  Sat, 13 Apr 2002 01:09:43 +0200

advi (1.0.0+cvs-2002.03.26-2) unstable; urgency=high

  * Added missing build dependencies (tetex-extra) (Closes: Bug#141438)

 -- Sven Luther <luther@debian.org>  Sat, 13 Apr 2002 01:09:43 +0200

advi (1.0.0+cvs-2002.03.26-1) unstable; urgency=high

  * Updated to CVS version, to solve the problem of executing embedded
    commands by default.
  * Added missing build dependencies (Closes: Bug#138129)
  * PS fonts are not supported (Bug#138157 retrograded to whishlist priority).
    I added a warning in the description and in the README.Debian files.

 -- Sven Luther <luther@debian.org>  Wed, 27 Mar 2002 11:45:42 +0100

advi (1.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Sven Luther <luther@debian.org>  Sun, 24 Feb 2002 15:37:58 +0100

activedvi (0.3.1-3) unstable; urgency=low

  * Don't strip mldvi executable (Closes: Bug#133568)

 -- Sven Luther <luther@debian.org>  Thu, 21 Feb 2002 17:37:32 +0100

activedvi (0.3.1-2) unstable; urgency=low

  * Fixed the manpage.

 -- Sven Luther <luther@debian.org>  Fri, 25 Jan 2002 13:42:10 +0100

activedvi (0.3.1-1) unstable; urgency=low

  * Renamed package.
  * Now based on ActiveDVI by Jun Furuse.

 -- Sven Luther <luther@debian.org>  Fri, 25 Jan 2002 12:23:38 +0100

mldvi (1.0-6) unstable; urgency=low

  * added xlibs-dev to build depends.
  * closes #108849.

 -- Sven Luther <luther@debian.org>  Thu, 16 Aug 2001 10:23:10 +0200

mldvi (1.0-5) unstable; urgency=low

  * added some build dependencies.
  * closes #108388.

 -- Sven Luther <luther@debian.org>  Wed, 15 Aug 2001 11:52:00 +0200

mldvi (1.0-4) unstable; urgency=low

  * rebuilt with ocaml 3.02.
  * installed man page correctly.

 -- Sven Luther <luther@debian.org>  Mon, 30 Jul 2001 20:20:13 +0200

mldvi (1.0-3) unstable; urgency=low

  * stripping ocaml binaries does not work, disabled it.

 -- Sven Luther <luther@debian.org>  Fri, 19 May 2000 12:42:08 +0200

mldvi (1.0-2) unstable; urgency=low

  * Fixed build problem for m68k.
  * Recompiled for ocaml 3.00.

 -- Sven Luther <luther@debian.org>  Fri, 12 May 2000 17:40:33 +0200

mldvi (1.0-1) unstable; urgency=low

  * Initial Release.

 -- Sven Luther <luther@debian.org>  Thu, 23 Mar 2000 18:13:05 +0100
